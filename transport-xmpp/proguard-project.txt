# This is a pretty standard optimization only proguard config for an
# Android Project

-keep class org.projectmaxs.shared.global.util.Log
-keep class org.projectmaxs.shared.maintransport.TransportInformation

-keepclasseswithmembers class * extends org.jivesoftware.smack.sasl.SASLMechanism {
    public <init>(org.jivesoftware.smack.SASLAuthentication);
}

################################################################################
# Custom proguard settings

# We don't use jzlib, but instead the Android API for compression
-dontnote com.jcraft.jzlib.*

-dontnote android.os.SystemProperties

-dontnote sun.security.pkcs11.SunPKCS11

# Include general Android proguard settings
-include ../build/proguard-project-shared.txt
